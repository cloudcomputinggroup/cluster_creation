terraform {
  # Optional attributes and the defaults function are
  # both experimental, so we must opt in to the experiment.
  experiments = [module_variable_optional_attrs]
}

variable "AZ_CLIENT_ID" {
    type = string
}
variable "AZ_CLIENT_SECRET" {
    type = string
}
variable "AZ_SUBSCRIPTION_ID" {
    type = string
}
variable "AZ_TENANT_ID" {
    type = string
}

variable "resource_group_data" {
    type = object({
        name = string
        location = string
    })
}

variable "aks_data" {
    type = object({
        cluster_name = string
        prefix = string
        node_pool_name = string
        node_count = number
        vm_size = string
        kubernetes_version = string
        tags = optional(map(string))
    })
}

variable "kubernetes_data"{
    type = object({
        namespace_name = string
        namespace_annotations = map(string)
        namespace_labels = map(string)
    })
}