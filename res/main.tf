resource "azurerm_resource_group" "az_rg" {
  name     = var.resource_group_data.name
  location = var.resource_group_data.location
}

resource "azurerm_kubernetes_cluster" "aks_cluster" {
  name                = var.aks_data.cluster_name
  location            = azurerm_resource_group.az_rg.location
  resource_group_name = azurerm_resource_group.az_rg.name
  dns_prefix = var.aks_data.prefix
  kubernetes_version = var.aks_data.kubernetes_version
  
  default_node_pool {
    name       = "default"
    node_count = var.aks_data.node_count
    vm_size    = var.aks_data.vm_size
    enable_auto_scaling = false
  }

  identity {
    type = "SystemAssigned"
  }

  tags = var.aks_data.tags
}


resource "kubernetes_namespace" "ns" {
  metadata {
    annotations = var.kubernetes_data.namespace_annotations
    labels = var.kubernetes_data.namespace_labels
    name = var.kubernetes_data.namespace_name
  }
  depends_on = [
    azurerm_kubernetes_cluster.aks_cluster
  ]
}
