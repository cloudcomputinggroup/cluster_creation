terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>3.2.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "~>2.10.0"
    }
  }
}


provider "azurerm" {
  features {}
  subscription_id = var.AZ_SUBSCRIPTION_ID
  client_id = var.AZ_CLIENT_ID
  client_secret = var.AZ_CLIENT_SECRET
  tenant_id = var.AZ_TENANT_ID
}


provider "kubernetes" {
  #config_path = azurerm_kubernetes_cluster.aks_cluster.kube_config_raw
  host = azurerm_kubernetes_cluster.aks_cluster.kube_config.0.host
  username = azurerm_kubernetes_cluster.aks_cluster.kube_config.0.username
  password = azurerm_kubernetes_cluster.aks_cluster.kube_config.0.password
  client_certificate = base64decode(azurerm_kubernetes_cluster.aks_cluster.kube_config.0.client_certificate)
  client_key = base64decode(azurerm_kubernetes_cluster.aks_cluster.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks_cluster.kube_config.0.cluster_ca_certificate)
}

