
resource_group_data = {
    name = "cccgroup"
    location = "North Europe"
}


aks_data = {
    cluster_name = "ccc-k8s-cluster"
    prefix = "cck8s"
    node_pool_name = "default"
    node_count = 1
    vm_size = "Standard_D2as_v4"
    kubernetes_version = "1.21.9"
    tags = {
        "cluster" = "test-k8s"
    }
}

kubernetes_data = {
    namespace_name = "cloudcomputing"
    namespace_labels = {
        app = "example-ns"
    }
    namespace_annotations = {
        app = "example-ns"
    }
}