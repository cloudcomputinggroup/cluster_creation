generate "backend"{
    path = "backend.tf"
    if_exists = "overwrite_terragrunt"
    contents = <<-EOF
    terraform{
        backend "azurerm" {
            storage_account_name = "${get_env("AZ_STORAGE_ACCOUNT_NAME", "")}"
            container_name       = "${get_env("AZ_STORAGE_CONTAINER_NAME", "")}"
            access_key           = "${get_env("AZ_STORAGE_ACCESS_KEY", "")}"
            key                  = "${format("%s/terraform.tfstate",get_env("CI_PROJECT_DIR",""))}"
        }
    }
    EOF
}